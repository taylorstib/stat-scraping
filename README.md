#ESPN stat scraping for nfl
Scrape.rb grabs the pages
pull_data.rb pages format the stats into a neat csv file. 
- Use the pull_data method that corresponds to the stat category you are interested in (passing, receiving, rushing)
- Receiving limits stats for top 240 receivers. (Working on making the whole thing dynamic so user can specify how many records are needed)


### Work in progress
- Want to make it a single pull_data script (check issues for specifics)
- Turn it into a gem
- Have the data hold pages labeled for the given season and week number
- Ability to scrabe past seasons??
